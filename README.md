## iHomeMonitor
This project is the final assignment for a IOT application assignment. For the assignment I had to create an IOT project to show use what I had learned in lectures. iHomemonitor is a responsive dashboard visualizes sensor data sent from hardware to the realtime-database. This app was created with create-react-app.

## Package manager
>yarn

### Installation
> yarn install

Consult admin for environment variables.

### Run
> yarn start

### Build
> yarn build

### Hardware
Raspberry pi 3 (Raspian)

Sensors:
* Adafruit SGP30 Air Quality Sensor Breakout - VOC and eCO2
* Adafruit Sensiron SHT31-D Temperature & Humidity Sensor Breakout
