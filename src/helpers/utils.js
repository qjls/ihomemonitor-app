export default (function() {
    let objProto = Object.prototype;
    let objToString = objProto.toString;
    const resourceTypes = {
        eco2: 'eCO2',
        temperature: 'temperature',
        humidity:'humidity',
        tvoc: 'TVOC',
    };

    return {
        resourceTypes: function (type){
            return resourceTypes[type];
        },
        isObject: function(obj) {
            return Object.isObject(obj);
        },
        isArray: function(obj) {
            return  objToString.call(obj) === '[object Array]';
        },
        isEmpty: function(obj) {
            return obj === undefined || obj === null;
        },
        map: function(obj, callback) {
            if (this.isEmpty(obj)) {
                throw Error('Cannot map empty object');
            }

            if (!this.isObject(obj) || !this.isArray(obj)) {
                throw Error('object must be either object or an array');
            }

            if (this.isArray(obj)) {
                return obj.map(callback);
            }

            let mappedArray = [];

            for (let key in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                    mappedArray.push(callback(obj[key], key));
                }
            }

            return mappedArray;

        },
        reduceByKey: function(data, resource) {
            if (!data || !resource) {
                return;
            }

            let reducedObject = {};

            let resourceKey = resourceTypes[resource];

            for (const [key, value] of Object.entries(data)) {
                const values = Object.values(value).map((value) => {
                    return {
                        measurements: {
                            [resourceKey]: value.measurements[resourceKey],
                        },
                        timestamp: value.timestamp,
                    };
                });

                reducedObject= Object.assign({}, reducedObject, {
                    [key]: values,
                });
            }

            return reducedObject;
        },
    };
})();