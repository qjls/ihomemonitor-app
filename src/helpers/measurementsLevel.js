export default (function () {
    const minimumLevels = {
        temperature: {
            high: 25,
            low: 16,
        },
        humidity: {
            high: 63,
            low: 56,
        },
        eCO2: {
            high: 500,
            low: 300,
        },
        TVOC: {
            high: 40,
            low: 10,
        },
    };

    return {
        isHigh: (value, resource) => {
            return value >= minimumLevels[resource].high;
        },
        isLow: (value, resource) => {
            return value <= minimumLevels[resource].low;
        },
    };
})();