import React, { createContext } from 'react';
import { FirebaseAppProvider, SuspenseWithPerf } from 'reactfire';
import Loading from '../../components/loader/Loader';
import PropTypes from 'prop-types';

const firebaseConfig = {
    apiKey: process.env.REACT_APP_FIREBASE_KEY,
    authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
    databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
    projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
    storageBucket: process.env.REACT_APP_FIREBASE_STORAGE,
    messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER,
    appId: process.env.REACT_APP_FIREBASE_APP_ID,
};

const GlobalContext = createContext({});

function Provider ({children}) {

    if (
        !firebaseConfig?.apiKey &&
		!firebaseConfig?.authDomain &&
		!firebaseConfig?.databaseURL &&
		!firebaseConfig?.projectId &&
		!firebaseConfig?.storageBucket &&
		!firebaseConfig?.messagingSenderId &&

		!firebaseConfig?.appId
    ) {
        console.error(
            'Please configure the env variables: apiKey,authDomain, databaseURL, projectId, storageBucket, messagingSenderId and appId'
        );

        return (
            <div id='environment-error'>
                <div className='error-message'>
					Please configure the env variables: <br />
                    <ul>
                        <li>apiKey</li>
                        <li>authDomain</li>
                        <li>databaseURL</li>
                        <li>projectId</li>
                        <li>storageBucket</li>
                        <li>messagingSenderId</li>
                        <li>appId</li>
                    </ul>
                </div>
            </div>
        );
    }

    return (
        <FirebaseAppProvider firebaseConfig={firebaseConfig}>
            <SuspenseWithPerf fallback={ <Loading backgroundColor="#00796B" color="#fff" /> } traceId='firebase-loading'>
                {children}
            </SuspenseWithPerf>
        </FirebaseAppProvider>
    );
}

Provider.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.object,
        PropTypes.array,
    ]).isRequired,
};

export default GlobalContext;

export { Provider };
