import { useEffect, useState } from 'react';
import { useDatabase } from 'reactfire';
import useDeviceId from './useDeviceId';

export default function useMeasurementData () {
    const deviceData = useDeviceId();
    const db = useDatabase();
    const [data, setData] = useState(null);
    const deviceRef = db.ref(`/device/${ deviceData.deviceId }/data`);
    let keys= [];

    useEffect(() => {
        if(!data) {
            deviceRef.on('value', (snapshot) => {
                setData(snapshot.toJSON());
            });
        }
    }, [deviceRef, data]);

    if (data) {
        keys = Object.keys(data);
    }

    return {
        data: data,
        keys: keys,
    };
}