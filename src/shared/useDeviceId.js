import { useFirestore, useFirestoreDocDataOnce, useUser, } from 'reactfire';

export default function useDeviceId() {
    const user= useUser();

    const store = useFirestore();

    const userRef = store.collection('user').doc(user?.uid);

    const { device } = useFirestoreDocDataOnce(userRef);

    return {
        deviceId: device || null,
    };
}