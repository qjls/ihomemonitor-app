import React, { useEffect, useState, } from 'react';
import PropTypes from 'prop-types';
import { CardSubtitle, CardText, CardTitle, Input, Row } from 'reactstrap';
import CardContainer from '../../components/card/CardContainer';
import { useUser } from 'reactfire';
import useMeasurementData from '../../shared/useMeasurementData';
import { useParams } from 'react-router-dom';
import utils from '../../helpers/utils';
import LineChart from '../../components/charts/LineChart';
import Loading from '../../components/loader/Loader';

function Detail () {
    const {type} = useParams();
    const [selectedDate, setSelectedDate] = useState();
    const [dates, setDates] = useState([]);
    const measurementData = useMeasurementData();
    const user = useUser();
    let data;


    if (measurementData?.data) {
        const dataArray = Object.values(measurementData?.data);
        const t = dataArray.map( dA => Object.values(dA));

        data = utils.reduceByKey(measurementData.data, type);


        console.log({t});
    }

    useEffect(() => {
        if(data && dates.length <= 0) {
            setDates(Object.keys(data));
        }
    }, [data, dates]);


    useEffect( () => {
        if(dates) {
            setSelectedDate(dates[0]);
        }

    }, [dates]);

    if (!user) {
        return null;
    }

    const {
        displayName,
        email,
    } = user;
    const currentMetrics =data?.[selectedDate];

    const labels = currentMetrics?.map(cM =>  cM.timestamp.replace(selectedDate, '').slice(0,-3)) ?? [];
    const metricData = currentMetrics?.map(cm => cm.measurements[utils.resourceTypes(type)]) ?? [];


    console.log({currentMetrics, selectedDate, labels, dates});

    if(!data) {
        return <Loading/>;
    }

    return (
        <div className='page detail'>
            <div className="page__inner">
                <CardTitle>{`Detail: ${type}`}</CardTitle>
                <CardText>{ `Logged in as ${ displayName ? displayName : email}` }</CardText>

                <CardContainer>
                    <CardSubtitle>timeseries</CardSubtitle>
                    <Row>
                        {'Date: '}
                        <Input type="select" name="select" onChange={event => setSelectedDate(event.target.value)} defaultValue={dates?.[1]}>
                            {  dates.map((date, index) => <option key={date} defaultChecked={index === 1}  value={date}>{date}</option>)}
                        </Input>
                    </Row>

                    <Row >
                        {selectedDate  && <LineChart labels={labels} data={metricData} />}
                    </Row>
                </CardContainer>
            </div>
        </div>
    );
}

Detail.propTypes = {
    type: PropTypes.string,
};

export default Detail;
