import React, { useState } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Content from '../components/layout/Content';
import DashBoard from './dashboard/DashBoard';
import Detail from './detail/Detail';
import Header from '../components/layout/Header';
import Login from './login/LoginContainer';
import PrivateRoute from '../components/privateRoute/PrivateRoute';
import ResetPassword from './reset-password/ResetPassword';
import Sidebar from '../components/layout/Sidebar';
import SignUp from './signup/SignUp';
import { Provider } from '../shared/context/GlobalContext';

function App() {
    const [isSidebarOpen, setIsSidebarOpen] = useState(false);

    function toggleSideBar () {
        setIsSidebarOpen(!isSidebarOpen);
    }

    return (
        <Provider>
            <BrowserRouter>
                <Header isSidebarOpen={ isSidebarOpen } toggleSideBar={ toggleSideBar }/>
                <main>
                    <Sidebar isSidebarOpen={ isSidebarOpen } toggleSideBar={ toggleSideBar } />
                    <Content>
                        <Switch>
                            <Route exact path='/login' component={ Login } />
                            <Route exact path='/sign-up' component={ SignUp } />
                            <Route exact path='/reset-password' component={ ResetPassword } />
                            <PrivateRoute path='/metric/:type'><Detail/></PrivateRoute>
                            <PrivateRoute path='/'><DashBoard/></PrivateRoute>
                        </Switch>
                    </Content>
                </main>
            </BrowserRouter>

        </Provider>
    );
}

export default App;
