import React, {  useEffect, useState } from 'react';

import { Card, CardBody, CardHeader, CardFooter, Button, Form } from 'reactstrap';

import CardSection from '../../components/card/CardSection';
import CardTitle from '../../components/card/CardTitle';
import ErrorMessage from '../../components/form/ErrorMessage';
import InputField from '../../components/form/InputField';
import SignUpImage from '../../components/assets/images/SignUpImage';
const emailValidation = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/);
const passwordValidation = new RegExp(/[a-zA-z0-9]{6,20}/);
import { useHistory } from 'react-router-dom';
import { useAuth, useFirestore, useFirestoreCollectionData, useUser } from 'reactfire';
import { Alert, ButtonGroup, Row } from 'reactstrap/lib';

const validators = {
    email: (value) => emailValidation.test(value) ?     false : 'Please enter a valid email',
    password: (value) => passwordValidation.test(value) ? false :  'Please enter a valid password',
    passwordCheck: (passwordCheck, password) => password === passwordCheck  ?  false : 'Passwords must match' ,
};

function SignUp () {
    const auth = useAuth();
    const user = useUser();
    const history = useHistory();
    const [credentials, setCredentials] = useState({});
    const store = useFirestore();

    const devices = useFirestoreCollectionData(store.collection('device'));

    const [error, setError] = useState({});
    const [alert, setAlert] = useState({
        active: false,
        message: null,
        color: 'primary',
    });


    if (user) {
        history.push('/');
    }

    useEffect(() => {
        if (alert.active) {
            setTimeout(() => {
                setAlert({
                    active: false,
                    message: null,
                    color: 'primary',
                });
            }, 3000);
        }
    },[alert]);

    function registerToDevice(user) {
        if (!user.uid) {
            return null;
        }

        try {
            store.collection('user').doc(user.uid).set({
                device: devices[0].id,
            });
        } catch (error) {
            console.error(error.message);
        }
    }


    function register () {
        event.preventDefault();
        if(!error.password && !error.passwordCheck && !error.email) {
            auth.createUserWithEmailAndPassword(
                credentials.email,
                credentials.password
            ).then(({user}) => {
                registerToDevice(user);
            }).catch((error) => {
                setAlert({
                    active: true,
                    message: error.message,
                    color: 'warning',
                });
            });
        }
    }


    function validate ({ name, value }) {
        const rule = validators[name];
        return rule(value, credentials.password);
    }

    function handleChange (event) {
        event.preventDefault();
        const {
            target: { name, value },
        } = event;


        setCredentials({
            ...credentials,
            [name]: value,
        });

        setError({
            ...error,
            [name]: null,
        });
    }

    function handleBlur (event) {
        event.preventDefault();

        const {
            target: { name },
        } = event;

        const isInvalid = validate(event.target);

        if  (isInvalid) {
            setError(prev => ({
                ...prev,
                [name]: isInvalid,
            }));
        }

    }

    return (
        <div className='page login'>
            <Card >
                <CardHeader>
                    <CardTitle>Sign Up</CardTitle>
                </CardHeader>

                <CardBody>
                    <CardSection className='login-form'>
                        <Form>
                            {alert.active &&
                                <Row>
                                    <Alert color={alert.color}>
                                        { alert.message }
                                    </Alert>
                                </Row>
                            }

                            <Row>
                                <InputField>
                                    <label htmlFor='email'>Email</label>
                                    <input
                                        type='email'
                                        name='email'
                                        id='email'
                                        autoFocus={true}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.email}
                                    </ErrorMessage>
                                </InputField>
                            </Row>
                            <Row>
                                <InputField>
                                    <label htmlFor='password'>Password</label>
                                    <input
                                        type='password'
                                        name='password'
                                        id='password'
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.password }
                                    </ErrorMessage>
                                </InputField>
                            </Row>
                            <Row>
                                <InputField>
                                    <label htmlFor='passwordCheck'>Password Confirmation</label>
                                    <input
                                        type='password'
                                        name='passwordCheck'
                                        id='passwordCheck'
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.passwordCheck }
                                    </ErrorMessage>
                                </InputField>
                            </Row>
                        </Form>
                    </CardSection>

                    <CardSection className='login-image'>
                        <SignUpImage />
                    </CardSection>
                </CardBody>
                <CardFooter>
                    <ButtonGroup>
                        <Button
                            className='white'
                            onClick={() => history.push('/login')}
                        >
                            Back to Login
                        </Button>

                        <Button
                            color="primary"
                            onClick={register}
                        >
                            Sign Up

                        </Button>
                    </ButtonGroup>
                </CardFooter>
            </Card>
        </div>
    );
}

export default SignUp;
