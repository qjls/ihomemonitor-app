import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'reactstrap';
import measurementsLevel from '../../../helpers/measurementsLevel';

function Tile ({resource, value}) {
    const isHigh = measurementsLevel.isHigh(value, resource);
    const isLow = measurementsLevel.isLow(value, resource);

    function getLevelStyle () {
        if (isHigh) {
            return 'high';
        }

        if (isLow) {
            return 'low';
        }

        return '';
    }

    return (
        <Col className='current-tile'>
            <div className={ `current-tile__inner ${ getLevelStyle() }` }>
                <div className={ `current-tile__value ${ getLevelStyle() }` }>{ value }</div>
                <div className='current-tile__resource'>{ resource }</div>
            </div>
        </Col>
    );
}

Tile.propTypes = {
    resource: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]).isRequired,
};

export default Tile;
