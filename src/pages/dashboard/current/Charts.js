import React from 'react';
import PropTypes from 'prop-types';
import TimeSeriesContainer from '../timeseries/TimeSeries';
import CardContainer from '../../../components/card/CardContainer';
import {
    Row,
} from 'reactstrap';

export default function Charts({data, recentDateKey}) {

    const labels = data.map(d => d.timestamp.replace(recentDateKey, '').slice(0,-3));
    const temperatureData = data.map( d => d.measurements['temperature']);
    const humidityData =  data.map( d => d.measurements['humidity']);
    const eco2Data = data.map( d => d.measurements['eCO2']);
    const tvocData = data.map( d => d.measurements['TVOC']);

    const metrics = [
        {

            resource: 'Temperature',
            data: temperatureData
        },
        {resource:'Humidity', data: humidityData},
        {resource: 'eCO2', data: eco2Data },
        {resource: 'TVOC', data: tvocData}
    ];

    return (
        <CardContainer>
            {metrics.map( metric =>
                <Row key={JSON.stringify(metric)}>
                    <TimeSeriesContainer data={ metric.data } labels={labels} resource={metric.resource} />
                </Row>
            )}
        </CardContainer>
    );


}

Charts.propTypes = {
    recentDateKey:  PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({
        timestamp: PropTypes.string,
        measurements:  PropTypes.shape({
            temperature: PropTypes.number,
            humidity:  PropTypes.number,
            eCO2: PropTypes.number,
            TVOC: PropTypes.number,
        })
    }))
};