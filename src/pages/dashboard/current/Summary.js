import React from 'react';
import PropTypes from 'prop-types';
import CardContainer from '../../../components/card/CardContainer';
import CurrentTile from './Tile';

/**
 * Takes current measurements and displays in the container.
 * @param {Object} props
 */

function Summary ({measurements}) {
    if (!measurements) {
        return null;
    }

    return (
        <CardContainer>
            <div className="current-tile__wrapper">
                {
                    Object.keys(measurements)
                        .reverse()
                        .map((key) => {
                            if (key === 'eCO2' || key === 'TVOC'){
                                return (
                                    <CurrentTile key={`${key}` }
                                        resource={ key }
                                        value={  parseInt(measurements[key], 10) }
                                    />
                                );
                            }


                            return (
                                <CurrentTile
                                    key={`${key}`}
                                    resource={ key }
                                    value={  parseFloat(measurements[key]).toFixed(1) }
                                />
                            );
                        })
                }
            </div>
        </CardContainer>
    );
}

Summary.defaultProps = {
    data: {
        temp: 25,
        humidity: 10,
        co2: 5,
        tvoc: 2,
    },
};

Summary.propTypes = {
    data: PropTypes.shape({
        temp: PropTypes.number,
        humidity: PropTypes.number,
        co2: PropTypes.number,
        tvoc: PropTypes.number,
    }),
    measurements: PropTypes.object,
};

export default Summary;
