import React, { useEffect, useState } from 'react';
import Charts from './current/Charts';
import Loading from '../../components/loader/Loader';
import Page from '../Page';
import Summary from './current/Summary';
import useMeasurementData from '../../shared/useMeasurementData';
import { useUser } from 'reactfire';

function DashBoard () {
    const user = useUser();
    const measurementData = useMeasurementData();

    const [
        measurements,
        setMeasurements,
    ] = useState(null);

    const recentDateKey = measurementData?.keys.slice(-1)[0];

    useEffect(() => {
        if (measurementData?.keys.length > 0) {
            const recentDate = measurementData?.data[recentDateKey];
            const lastDateKey = Object.keys(recentDate).slice(-1)[0];

            const current = recentDate[lastDateKey];

            setMeasurements(current['measurements']);

        }
    }, [
        measurementData,
        recentDateKey
    ]);

    if(!measurementData?.data) {
        return <Loading/>;
    }

    const data = Object.values(measurementData.data[recentDateKey]);

    return (
        <Page>
            <div>
                <div className='page__caption'>
                    {`Welcome back ${
                        user?.displayName ?? user?.email
                    }`}
                </div>
                <Summary measurements={ measurements }/>
                <Charts data={ data } recentDateKey={recentDateKey}/>

            </div>
        </Page>
    );
}

export default DashBoard;
