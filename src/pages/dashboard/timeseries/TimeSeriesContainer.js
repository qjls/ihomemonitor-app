import React from 'react';
import TimeSeries from './TimeSeries';
import PropTypes from 'prop-types';

function TimeSeriesContainer ({data, ...props}) {
    if (!data) {
        return null;
    }

    return <TimeSeries { ...props }/>;
}

TimeSeriesContainer.propTypes = {
    data: PropTypes.object,
};

export default TimeSeriesContainer;
