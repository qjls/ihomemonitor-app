import React from 'react';
import PropTypes from 'prop-types';
import LineChart from '../../../components/charts/LineChart';

function TimeSeries ({data, labels, resource}) {
    return <LineChart data={ data } labels={ labels } label={ `${ resource } today`} resource={ resource } />;
}

TimeSeries.defaultProps = {
    data: [],
    labels: [],
};

TimeSeries.propTypes = {
    data: PropTypes.arrayOf(PropTypes.number),
    labels: PropTypes.arrayOf(PropTypes.string),
    resource: PropTypes.oneOf([
        'Temperature',
        'TVOC',
        'eCO2',
        'Humidity',
    ]).isRequired,
};

export default TimeSeries;
