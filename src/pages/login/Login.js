import React, {  useEffect, useState } from 'react';

import { Button, Card, CardBody, CardHeader, CardFooter, Form, Row } from 'reactstrap';

import LoginImage from '../../components/assets/images/LoginImage';
import ErrorMessage from '../../components/form/ErrorMessage';
import CardSection from '../../components/card/CardSection';
import InputField from '../../components/form/InputField';
import CardTitle from '../../components/card/CardTitle';
const emailValidation = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/);
const passwordValidation = new RegExp(/[a-zA-z0-9]{6,20}/);
import { useHistory } from 'react-router-dom';
import { useAuth, useUser } from 'reactfire';
import { NavLink } from 'react-router-dom';
import { Alert, ButtonGroup } from 'reactstrap/lib';

const validators = {
    email: (value) => emailValidation.test(value) ,
    password: (value) => passwordValidation.test(value),
};

function Login () {
    const auth = useAuth();
    const user = useUser();
    const history = useHistory();
    const [credentials, setCredentials] = useState({});

    const [error, setError] = useState({});
    const [alert, setAlert] = useState({
        active: false,
        message: null,
        color: 'primary',
    });


    if(user) {
        history.push('/');
    }

    useEffect(() => {
        if (alert.active) {
            setTimeout(() => {
                setAlert({
                    active: false,
                    message: null,
                    color: 'primary',
                });
            }, 3000);
        }
    },[alert]);

    function signIn () {
        event.preventDefault();

        auth.signInWithEmailAndPassword(
            credentials.email,
            credentials.password
        ).then(() => {
            history.push('/dashboard');
        }).catch((error) => {
            setAlert({
                active: true,
                message: error.message,
                color: 'warning',
            });
        });
    }

    function validate ({ name, value }) {
        const rule = validators[name];

        if (!value || value === '') {
            return false;
        }

        return rule(value, credentials[name]);
    }

    function handleChange (event) {
        event.preventDefault();
        const {
            target: { name, value },
        } = event;

        setCredentials({
            ...credentials,
            [name]: value,
        });

        setError({});
    }

    function handleBlur (event) {
        event.preventDefault();

        const {
            target: { name, value },
        } = event;

        console.log(event.target.value);

        const isValid = validate(event.target);

        if (!isValid && value) {
            console.log('is not valid');
            setError({
                ...error,
                [name]: `Please enter a valid ${name}`,
            });
        }
    }

    return (
        <div className='page login'>
            <Card>
                <CardHeader>
                    <CardTitle>Login</CardTitle>
                </CardHeader>

                <CardBody>
                    <CardSection className='login-form'>
                        <Form>
                            {alert.active &&
                                <Row>
                                    <Alert color={alert.color}>
                                        { alert.message }
                                    </Alert>
                                </Row>
                            }
                            <Row>
                                <InputField>
                                    <label htmlFor='email'>Email</label>
                                    <input
                                        type='email'
                                        name='email'
                                        id='email'
                                        autoFocus={true}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.email}
                                    </ErrorMessage>
                                </InputField>
                            </Row>

                            <Row>
                                <InputField>
                                    <label htmlFor='password'>Password</label>
                                    <input
                                        type='password'
                                        name='password'
                                        id='password'
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.password }
                                    </ErrorMessage>
                                </InputField>
                            </Row>

                            <Row>
                                <NavLink exact to='/reset-password' className='link'>
                                    Forgot your password?
                                </NavLink>
                            </Row>

                            <Row>
                                <NavLink exact to='/sign-up' className='link'>
                                    Don&apos;t have an account? Sign up!
                                </NavLink>
                            </Row>
                        </Form>
                    </CardSection>

                    <CardSection className='login-image'>
                        <LoginImage />
                    </CardSection>
                </CardBody>

                <CardFooter>
                    <ButtonGroup>
                        <Button
                            color="primary"
                            onClick={signIn}
                        >
                            Login
                        </Button>
                    </ButtonGroup>
                </CardFooter>
            </Card>
        </div>
    );
}

export default Login;
