import React, {  useEffect, useState } from 'react';

import { Card, CardBody, CardHeader, CardFooter, Button, Form } from 'reactstrap';

import CardSection from '../../components/card/CardSection';
import CardTitle from '../../components/card/CardTitle';
import ErrorMessage from '../../components/form/ErrorMessage';
import InputField from '../../components/form/InputField';
import ResetPasswordImage from '../../components/assets/images/ResetPasswordImage';
const emailValidation = new RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)+$/);
import { useHistory } from 'react-router-dom';
import { useAuth, useUser } from 'reactfire';
import { Alert, ButtonGroup, Row } from 'reactstrap/lib';

const validators = {
    email: (value) => emailValidation.test(value) ?     false : 'Please enter a valid email',
};

function ResetPassword () {
    const auth = useAuth();
    const user = useUser();
    const history = useHistory();
    const [credentials, setCredentials] = useState({});
    const [alert, setAlert] = useState({
        active: false,
        message: null,
        color: 'primary',
    });

    const [error, setError] = useState({});

    if (user) {
        history.push('/');
    }

    useEffect(() => {
        if (alert.active) {
            setTimeout(() => {
                setAlert({
                    active: false,
                    message: null,
                    color: 'primary',
                });
            }, 3000);
        }
    },[alert]);

    function resetPass () {
        event.preventDefault();
        if(!error.email) {
            auth.sendPasswordResetEmail(
                credentials.email,
            ).then(() => {
                setAlert({
                    active: true,
                    message: 'You\'re password has been reset. Please check your email for further instructions.',
                    color: 'success',
                });
            }).then(() => {
                setTimeout(history.push('/login'), 5000);
            }).catch((error) => {
                setAlert({
                    active: true,
                    message: error.message,
                    color: 'warning',
                });
            });
        }
    }

    function validate ({ name, value }) {
        const rule = validators[name];
        return rule(value, credentials.password);
    }

    function handleChange (event) {
        event.preventDefault();
        const {
            target: { name, value },
        } = event;


        setCredentials({
            ...credentials,
            [name]: value,
        });
    }

    function handleBlur (event) {
        event.preventDefault();

        const {
            target: { name },
        } = event;

        const isValid = validate(event.target);

        setError(prev => ({
            ...prev,
            [name]: isValid,
        }));
    }

    return (
        <div className='page login'>
            <Card>
                <CardHeader>
                    <CardTitle>Reset password</CardTitle>
                </CardHeader>

                <CardBody>
                    <CardSection className='login-form'>
                        <Form>
                            {alert.active &&
                                <Row>
                                    <Alert color={alert.color}>
                                        { alert.message }
                                    </Alert>
                                </Row>
                            }

                            <Row>
                                <InputField>
                                    <label htmlFor='email'>Email</label>
                                    <input
                                        type='email'
                                        name='email'
                                        id='email'
                                        autoFocus={true}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                    />

                                    <ErrorMessage>
                                        {error?.email}
                                    </ErrorMessage>
                                </InputField>
                            </Row>
                        </Form>
                    </CardSection>

                    <CardSection className='login-image'>
                        <ResetPasswordImage />
                    </CardSection>
                </CardBody>

                <CardFooter>
                    <ButtonGroup>
                        <Button
                            className='white'
                            onClick={() => history.push('/login')}
                        >
                            Back to Login
                        </Button>

                        <Button
                            color="primary"
                            onClick={resetPass}
                        >
                        Reset password
                        </Button>

                    </ButtonGroup>
                </CardFooter>
            </Card>
        </div>
    );
}

export default ResetPassword;
