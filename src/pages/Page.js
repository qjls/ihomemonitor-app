import React, { Suspense } from 'react';
import PropTypes from 'prop-types';
import { AuthCheck} from 'reactfire';
import Loader from '../components/loader/Loader';
import Login from './login/LoginContainer';

function Page ({children}) {
    return (
        <Suspense fallback={<Loader />}>
            <AuthCheck fallback={<Login />}>
                <div className='page'>
                    { children }
                </div>
            </AuthCheck>
        </Suspense>
    );
}

Page.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.array,
        PropTypes.object,
    ]).isRequired,
};

export default Page;