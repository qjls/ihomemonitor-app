import CO2Icon from './CO2Icon';
import HumidityIcon from './HumidityIcon';
import TemperatureIcon from './TemperatureIcon';
import ToxicIcon from './ToxicIcon';

export {
    CO2Icon,
    HumidityIcon,
    TemperatureIcon,
    ToxicIcon,
};