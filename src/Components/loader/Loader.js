import React from 'react';
import PropTypes from 'prop-types';

function Loading({backgroundColor, color}) {
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: backgroundColor ? backgroundColor :'#fff',
                color: color ? color : 'inherit' ,
                minHeight: '100vh',
                width: '100%',
            }}
        >
            <div className={`lds-ring ${backgroundColor ? '' : 'primary'}`}><div></div><div></div><div></div><div></div></div>
        </div>
    );
}

Loading.defaultProps = {
    backgroundColor: undefined,
    color: undefined
};

Loading.propTypes = {
    backgroundColor: PropTypes.string,
    color: PropTypes.string
};

export default Loading;
