import React from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Logo } from '../icons/logo.svg';
import { ReactComponent as MenuIcon } from '../icons/menu-24px.svg';
import { Navbar } from 'reactstrap';

import LogoutIcon from '../assets/icons/login/LogoutIcon';
import LoginIcon from '../assets/icons/login/LoginIcon';
import { NavLink } from 'react-router-dom';
import { useUser, useAuth } from 'reactfire';


function Header ({toggleSideBar}) {
    const currentUser = useUser();
    const auth = useAuth();


    function logOut () {
        try {
            auth.signOut().then(() => {
                history.go('/login');
            });
        } catch (error) {
            throw new Error(error);
        }
    }

    return (
        <div className='header'>
            <div className='header-inner'>
                <Navbar>
                    <div
                        className='header__menu'
                        onClick={ toggleSideBar }
                    >
                        <MenuIcon className='menu' />
                    </div>

                    <div className='header__logo'>
                        <Logo />
                    </div>

                    { currentUser ?
                        <div id='logout-btn' className='header__login' onClick={logOut}>
                            <LogoutIcon />
                        </div>
                        :
                        <div id='logout-btn' className='header__login' onClick={logOut}>
                            <NavLink exact to='/'>
                                <LoginIcon />
                            </NavLink>
                        </div>
                    }
                </Navbar>
            </div>
        </div>
    );
}

Header.propTypes = {
    toggleSideBar: PropTypes.func.isRequired,
};

export default Header;
