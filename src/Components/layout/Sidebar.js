import React from 'react';
import PropTypes from 'prop-types';
import { NavLink }from 'react-router-dom';
import { CO2Icon, HumidityIcon, TemperatureIcon, ToxicIcon }from '../assets/icons/measurements';
import DashIcon from '../assets/icons/menu/DashIcon';
import Nav from 'reactstrap/lib/Nav';
import NavItem from 'reactstrap/lib/NavItem';
import { useUser } from 'reactfire';

function Sidebar ({isSidebarOpen}) {
    const user = useUser();

    if (!user) {
        return null;
    }

    return (
        <aside id='sidebar' className={ `sidebar ${ isSidebarOpen ? 'open' : '' }` }>
            <div className='sidebar-inner'>
                <Nav vertical>
                    <NavItem >
                        <NavLink exact to='/' className='nav-link'>
                            <div className='nav__icon'><DashIcon /></div>
                            Dashboard
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/metric/temperature' className='nav-link'>
                            <div className='nav__icon'><TemperatureIcon /></div>

                            Temperature
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/metric/humidity' className='nav-link'>
                            <div className='nav__icon'><HumidityIcon /></div>
                            Humidity
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/metric/eco2' className='nav-link'>
                            <div className='nav__icon'><CO2Icon /></div>

                            Air quality
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink exact to='/metric/tvoc' className='nav-link'>
                            <div className='nav__icon'><ToxicIcon /></div>
                            Toxic levels
                        </NavLink>
                    </NavItem>
                </Nav>
            </div>
        </aside>
    );
}

Sidebar.propTypes = {
    isSidebarOpen: PropTypes.bool,
};

export default Sidebar;
