import React from 'react';
import PropTypes from 'prop-types';

function CardSection ({className, children}) {
    return <div className={`card_section ${className ? className : ''}`}>{children}</div>;
}

CardSection.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.array,
        PropTypes.arrayOf(PropTypes.object),
    ]),
    className: PropTypes.string,
};

export default CardSection;
