import React from 'react';
import PropTypes from 'prop-types';

function CardContent () {
    return <div className='card__content'>{children}</div>;
}

CardContent.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.array,
        PropTypes.arrayOf(PropTypes.object),
    ]),
};

export default CardContent;
