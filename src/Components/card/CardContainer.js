import React from 'react';
import PropTypes from 'prop-types';

const CardContainer = (props) => {
    const {
        children,
    } = props;

    return <div className='card-container'>{ children }</div>;
};

CardContainer.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.object,
        PropTypes.array,
    ]).isRequired,
};

export default CardContainer;