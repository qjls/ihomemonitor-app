import React from 'react';
import PropTypes from 'prop-types';

function ErrorMessage ({children}) {

    return <div className='error'>{ children }</div>;
}

ErrorMessage.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.array,
        PropTypes.object,
    ]),
};

export default ErrorMessage;
