import React from 'react';
import PropTypes from 'prop-types';

function InputField ({children}) {
    return <div className='form__input-field'>{children}</div>;
}

InputField.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.array,
        PropTypes.arrayOf(PropTypes.object),
    ]).isRequired,
};

export default InputField;
