import React from 'react';
import PropTypes from 'prop-types';

function Button () {

    return <div className="button" onClick={ onClick }>{ label }</div>;
}

Button.propTypes= {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

export default Button;
