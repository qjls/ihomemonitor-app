import React from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

function LineChart ({ data,label, labels, resource}) {
    const datasets =  [
        {
            backgroundColor: 'blue',
            hoverbackgroundColor: '#FFAA00',
            fill: false,
            data
        }
    ];

    return (
        <Line
            height={350}
            data={ {
                labels,
                datasets,
            } }
            options={{
                legend:{
                    display: false,
                    position:'top',
                },
                label: {
                    display: true,
                },

                maintainAspectRatio: false,
                responsive: true,
                scales: {
                    x: {
                        display: true,
                        title: {
                            display: true,
                            text: 'Time',
                            padding: {top: 30, left: 0, right: 0, bottom: 0}
                        },
                    },
                    y: {
                        display: true,
                        title: {
                            display: !!resource,
                            text: { resource },
                            padding: {top: 30, left: 0, right: 0, bottom: 0}
                        },
                    },
                },
                title:{
                    display:true,
                    text: label ? label :'' ,
                    fontSize:20,
                    fontStyle:'capitalize',
                },
            }}
        />
    );
}

LineChart.defaultProps = {
    data: [],
    labels: [],
    label:undefined ,
    resource: undefined
};

LineChart.propTypes = {
    data: PropTypes.arrayOf(PropTypes.number),
    labels: PropTypes.arrayOf(PropTypes.string),
    label: PropTypes.string,
    resource: PropTypes.string,
};

export default LineChart;
